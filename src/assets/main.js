import '@json-editor/json-editor';
import 'bootstrap/dist/css/bootstrap.min.css';

JSONEditor.defaults.options.theme = 'bootstrap4';
JSONEditor.defaults.options.iconlibs = 'bootstrap4';
JSONEditor.defaults.options.disable_edit_json = true;
JSONEditor.defaults.options.disable_properties = true;

const editor = new JSONEditor(document.getElementById('editor_holder'), {
  schema: {
    title: 'Dipendente',
    type: 'object',
    properties: {
      name: {
        title: 'Nome e cognome',
        type: 'string',
        minLength: 4,
        default: 'John Doe'
      },
      skills: {
        type: 'array',
        title: 'Competenze',
        items: {
          type: 'string',
          title: 'Competenza'
        }
      },
      languages: {
        type: 'array',
        title: 'Lingue',
        items: {
          type: 'object',
          title: 'Lingua',
          properties: {
            name: {
              type: 'string',
              title: 'Nome lingua'
            },
            spoken: {
              type: 'string',
              enum: ['A1', 'A2', 'B1', 'B2', 'C1', 'C2'],
              title: 'Livello parlato'
            },
            written: {
              type: 'string',
              enum: ['A1', 'A2', 'B1', 'B2', 'C1', 'C2'],
              title: 'Livello scritto'
            }
          }
        }
      },
      education: {
        type: 'array',
        title: 'Percorso di studi',
        items: {
          type: 'object',
          title: 'Istruzione',
          properties: {
            year: {
              type: 'string',
              title: 'Anno'
            },
            qualification: {
              type: 'string',
              title: 'Titolo'
            },
            shortDescription: {
              type: 'string',
              title: 'Descrizione'
            }
          }
        }
      },
      experiences: {
        type: 'array',
        title: 'Esperienze lavorative',
        items: {
          type: 'object',
          title: 'Esperienza',
          properties: {
            start: {
              type: 'string',
              title: 'Data inizio'
            },
            end: {
              type: 'string',
              title: 'Data  fine'
            },
            company: {
              type: 'string',
              title: 'Azienda'
            },
            tasks: {
              type: 'array',
              title: 'Incarichi / Attività',
              items: {
                type: 'object',
                title: 'Incarico / Attività',
                properties: {
                  titolo: {
                    type: 'string',
                    title: 'Mansione'
                  },
                  task: {
                    type: 'object',
                    title: 'Attività',
                    properties: {
                      title: {
                        type: 'string',
                        title: 'Nome attività'
                      },
                      description: {
                        type: 'string',
                        title: 'Descrizione attività'
                      }
                    }
                  },
                  environment: {
                    type: 'array',
                    title: 'Ambienti',
                    items: {
                      type: 'object',
                      title: 'Ambiente',
                      properties: {
                        title: {
                          type: 'string',
                          title: 'Nome ambiente'
                        },
                        description: {
                          type: 'string',
                          title: 'Descrizione ambiente'
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
});

document
  .getElementById('cLog')
  .addEventListener('click', () => console.log(editor.getValue()));
